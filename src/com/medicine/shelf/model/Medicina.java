package com.medicine.shelf.model;

import java.util.Date;

public class Medicina {

	public Medicina(String nombre, String tama�o, boolean pediatrica,
			Date fechaAdquisicion, Date caducidad, String stock, String dosis) {
		super();
		this.nombre = nombre;
		this.tama�o = tama�o;
		this.pediatrica = pediatrica;
		this.fechaAdquisicion = fechaAdquisicion;
		this.caducidad = caducidad;
		this.stock = stock;
		this.dosis = dosis;
	}

	// nombre comercial de la medicina
	private String nombre;
	
	// tama�o del frasco, cantidad de tabletas, etc.
	private String tama�o;

	private boolean pediatrica;
	
	private Date fechaAdquisicion;
	
	private Date caducidad;

	// cuantas tabletas quedan, o cuantos ml sobran,
	// o cuantos frascos hay
	private String stock;
	
	// cuantas veces al dia se toma la medicina
	private String dosis;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTama�o() {
		return tama�o;
	}

	public void setTama�o(String tama�o) {
		this.tama�o = tama�o;
	}

	public boolean isPediatrica() {
		return pediatrica;
	}

	public void setPediatrica(boolean pediatrica) {
		this.pediatrica = pediatrica;
	}

	public Date getFechaAdquisicion() {
		return fechaAdquisicion;
	}

	public void setFechaAdquisicion(Date fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}

	public Date getCaducidad() {
		return caducidad;
	}

	public void setCaducidad(Date caducidad) {
		this.caducidad = caducidad;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getDosis() {
		return dosis;
	}

	public void setDosis(String dosis) {
		this.dosis = dosis;
	}

}
