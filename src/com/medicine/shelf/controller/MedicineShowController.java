package com.medicine.shelf.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.medicine.shelf.model.Medicina;

public class MedicineShowController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public MedicineShowController() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SimpleDateFormat hoy = new SimpleDateFormat("EEE MMMM yyyy");
		GregorianCalendar cal = new GregorianCalendar(2012, GregorianCalendar.MARCH, 1);
		
		List<Medicina> list = new LinkedList<Medicina>();
		
		list.add(new Medicina("Tempra", "Frasco con 200ml", true, 
				cal.getTime(), new Date(), "1 frasco", "27gts c/ 6hrs cada que tenga fiebre"));
		list.add(new Medicina("Metamizol sodico (NeoMelubrina)", "Frasco con 60ml", true, 
				cal.getTime(), new Date(), "1 frasco", "27gts c/ 6hrs cada que tenga fiebre"));
		list.add(new Medicina("Parecetamol", "gotero con 60ml", true, 
				cal.getTime(), new Date(), "1 frasco", "27gts c/ 6hrs cada que tenga fiebre"));
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html><body>" +
				"Estas son las medicinas que tienes actualmente disponibles en casa:" +
				"<table border=1>");
		
		for(Medicina medicina: list){
			out.println("<tr><td>"+ medicina.getNombre() + "</td>" +
					"<td>"+
					// notar que se esta mandando un form con un chorro de campos tipo hidden 
					"<form method=POST action='showdetails.do'>" +
					"<input type=hidden name=_nombre_medicina value=\"" + medicina.getNombre() + "\" >" +
					"<input type=submit value=\"see details\" >" +
					"</form>" + 
					"<td>" +
					"</tr>");
			
			// q falta de saber de los campos hidden y de los campos de los formularios
		}
		
		out.println("</table></body></html>");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
